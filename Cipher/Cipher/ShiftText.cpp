#include "ShiftText.h"

ShiftText::ShiftText(const std::string text, const int key) : PlainText(text)
{
	this->_key = key;
	this->encrypt();
}

ShiftText::~ShiftText()
{
	this->_key = 0;
}

std::string ShiftText::encrypt()
{
	std::string encrypted = "";
	char ch = 0;
	int key = this->_key % ENGLISH_LETTERS_NUM; // if key is bigger than the number of letters

	for (unsigned int i = 0; i < (this->_text).length(); i++)
	{
		ch = this->_text[i];
		if (this->_text[i] >= 'a' && this->_text[i] <= 'z')
		{ // calculating the shift of the current letter
			ch = char(int(ENGLISH_LETTERS_NUM + ((_text[i] + key - 'a') % ENGLISH_LETTERS_NUM)) % ENGLISH_LETTERS_NUM + 'a');
		}
		encrypted += ch; // adding letter to string
	}
	this->_isEncrypted = true;
	this->_text = encrypted;

	return encrypted;
}

std::string ShiftText::decrypt()
{
	std::string decrypted = "";
	int srcKey = _key;

	this->_key = _key * (-1); // sending the oposite key
	decrypted = this->encrypt();
	this->_key = srcKey;

	this->_isEncrypted = false;
	this->_text = decrypted;

	return decrypted;
}

std::ostream& operator<<(std::ostream& out, ShiftText& s)
{
	std::cout << "Shift" << std::endl;

	if (!s._isEncrypted)
	{
		out << s.encrypt();
		s.decrypt();
	}
	else {
		out << s._text;
	}

	return out;
}