#ifndef SHIFTTEXT_H
#define SHIFTTEXT_H

#include "PlainText.h"

class ShiftText : public PlainText
{
private:
	int _key;
public:
	ShiftText(const std::string text, const int key);
	~ShiftText();
	std::string encrypt(); // return and change to encrypted string
	std::string decrypt(); // return and change to decrypted string
	friend std::ostream& operator<<(std::ostream& out, ShiftText& s); // return encrypted string
};

#endif