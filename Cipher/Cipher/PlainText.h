#ifndef PLAINTEXT_H
#define PLAINTEXT_H

#include <iostream>
#include <fstream>
#include <string>

#define ENGLISH_LETTERS_NUM 26
#define CAESAR_KEY 3


class PlainText
{
protected:
	std::string _text;
	bool _isEncrypted;
public:
	static int _NumOfTexts; // to count number of calls
	PlainText(const std::string text);
	~PlainText();
	bool isEncrypted() const; // return if encrypted
	std::string getText() const; // return text
};


#endif