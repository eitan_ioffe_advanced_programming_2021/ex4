#ifndef CAESAR_H
#define CAESAR_H

#include "ShiftText.h"

class CaesarText : public ShiftText
{
private:
	int _key;
public:
	CaesarText(const std::string text);
	~CaesarText();
	std::string encrypt(); // return and change to encrypted string
	std::string decrypt(); // return and change to  decrypted string
	friend std::ostream& operator<<(std::ostream& out, CaesarText& c); // return encrypted string
};

#endif