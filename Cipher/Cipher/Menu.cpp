#include "Menu.h"

void mainMenu()
{
	int choice = 1, key = 0;
	std::string str = "", fileName = "";

	while (choice != EXIT)
	{
		optionMenu(choice); // printing main options
		switch (choice)
		{
		case CIPHEN_STRING:
			std::cout << "Enter string: " << std::endl; 
			getline(std::cin, str);
			std::cout << decrypt(str) << std::endl;
			break;

		case CIPHEN_FILE:
			std::cout << "Enter txt file name: ";
			getline(std::cin, fileName);
			str = FileHelper::readFileToString(fileName); // getting string from file

			// encrypting / decrypting string
			std::cout << "Would you like to encrypt / decrypt? (1 - encrypt, 2 - decrypt): " << std::endl;
			checkInput(choice, ENCRYPT, DECRYPT);

			if (choice == ENCRYPT) {
				str = encrypt(str);
			}
			else if (choice == DECRYPT) {
				str = decrypt(str);
			}

			// saving / displaying string
			std::cout << "Would you like to save / display the output? (1 - save, 2 - output): " << std::endl;
			checkInput(choice, SAVE, DISPLAY);

			if (choice == SAVE)
			{
				std::cout << "Enter file name: ";
				getline(std::cin, fileName);
				std::ofstream out(fileName);
				if (out.is_open())
				{ // writing string to file
					out << str;
					out.close();
				}
			}
			else if (choice == DISPLAY)
			{
				std::cout << str << std::endl;
			}
			break;

		case STATIC_COUNT:
			std::cout << PlainText::_NumOfTexts << std::endl; // printing static variable
			break;

		case EXIT:
			break;

		default:
			std::cout << "Wrong choice, try again." << std::endl;
			break;
		}
	}
}

void optionMenu(int& n)
{
	std::cout << "Cipher Menu" << std::endl;
	std::cout << "1 - decrypt a string" << std::endl;
	std::cout << "2 - encrypt or decrypt a file" << std::endl;
	std::cout << "3 - number of outputs" << std::endl;
	std::cout << "4 - exit" << std::endl;
	std::cout << "Enter your choice: ";
	std::cin >> n;
	getchar(); // cleaning buffer
}


std::string decrypt(std::string str)
{
	std::string decrypted = "";
	int key = 0, choice = 0;
	std::cout << "How would you like to decrypt? (1 - custom shift, 2 - Caesar, 3 - Substitution)" << std::endl;
	checkInput(choice, SHIFT, SUB);

	if (choice == SHIFT)
	{
		std::cout << "Enter key: ";
		std::cin >> key;
		ShiftText shft(str, key);
		shft.decrypt();
		shft.decrypt();
		decrypted = shft.getText();
	}
	else if (choice == CAESAR)
	{
		CaesarText csr(str);
		csr.decrypt();
		csr.decrypt();
		decrypted = csr.getText();
	}
	else if (choice == SUB)
	{
		SubstitutionText sub(str, DEFULT_SUB_FILE);
		sub.decrypt();
		sub.decrypt();
		decrypted = sub.getText();
	}

	return decrypted;
}


std::string encrypt(std::string str)
{
	std::string encrypted = "";
	int key = 0, choice = 0;
	std::cout << "How would you like to encrypt? (1 - custom shift, 2 - Caesar, 3 - Substitution)" << std::endl;
	checkInput(choice, SHIFT, SUB); // if legal input

	if (choice == SHIFT)
	{
		std::cout << "Enter key: ";
		std::cin >> key;
		ShiftText shft(str, key);
		encrypted = shft.getText();
	}
	else if (choice == CAESAR)
	{
		CaesarText csr(str);
		encrypted = csr.getText();
	}
	else if (choice == SUB)
	{
		SubstitutionText sub(str, DEFULT_SUB_FILE);
		encrypted = sub.getText();
	}

	return encrypted;
}

void checkInput(int& n, int min, int max)
{
	std::cin >> n;
	getchar(); // clean buffer
	while (n < min || n > max)
	{
		std::cout << "Try again." << std::endl;
		std::cin >> n;
		getchar(); // clean buffer
	}
}
