#include "FileHelper.h"

std::string FileHelper::readFileToString(std::string fileName)
{
	std::ifstream file(fileName);
	std::stringstream buffer;
	if (file.is_open())
	{
		buffer << file.rdbuf(); // reading file into the buffer
		file.close();
	}

	return buffer.str(); // returning buffer as a string
}

void FileHelper::writeWordsToFile(std::string inputFileName, std::string outputFileName)
{
	std::ofstream out(outputFileName);
	std::string str = readFileToString(inputFileName); // reading input file conent
	std::istringstream iss(str);
	std::string word;

	if (out.is_open())
	{
		while (iss >> word) { // copying each word from string to a different line in the output file
			out << word << '\n';
		}
		out.close();
	}
}
