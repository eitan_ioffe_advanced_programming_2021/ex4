#include "PlainText.h"

int PlainText::_NumOfTexts = 0;

PlainText::PlainText(const std::string text)
{
	this->_text = text;
	this->_isEncrypted = false;
	this->_NumOfTexts++; // increasing by 1 every call
}

PlainText::~PlainText()
{
	this->_text = "";
	this->_isEncrypted = false;
}

bool PlainText::isEncrypted() const
{
	return this->_isEncrypted;
}

std::string PlainText::getText() const
{
	return this->_text;
}

