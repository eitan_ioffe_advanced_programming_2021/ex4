#ifndef FILEHELPER_H
#define FILEHELPER_H

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

class FileHelper
{
public:
	static std::string readFileToString(const std::string fileName); // return string with file's content
	static void writeWordsToFile(const std::string inputFileName, const std::string outputFileName); // writing src file to dst file word by word
};

#endif