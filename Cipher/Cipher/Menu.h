#ifndef MENU_H
#define MENU_H

#include "PlainText.h"
#include "SubstitutionText.h"
#include "FileHelper.h"
#include "Caesar.h"
#include "ShiftText.h"
#include <iostream>
#include <string.h>

#define CIPHEN_STRING 1
#define CIPHEN_FILE 2
#define STATIC_COUNT 3
#define EXIT 4

#define SHIFT 1
#define CAESAR 2
#define SUB 3

#define ENCRYPT 1
#define DECRYPT 2

#define SAVE 1
#define DISPLAY 2

#define DEFULT_SUB_FILE "dictionary.csv"

void mainMenu(); // manue
void optionMenu(int& n); // chosing the option
std::string decrypt(std::string str); // decrypting options - return decrypted string
std::string encrypt(std::string str); // encrypting options - return encrypted string
void checkInput(int& n, int min, int max); // checking that n is between min and max

#endif