#include "SubstitutionText.h"

SubstitutionText::SubstitutionText(const std::string text, const std::string dictionaryFileName) : PlainText(text)
{
	this->_dictionaryFileName = dictionaryFileName;
	this->encrypt();
}

SubstitutionText::~SubstitutionText()
{
	this->_dictionaryFileName = "";
}

std::string SubstitutionText::encrypt()
{
	std::string line;
	std::string encrypted = "";
	std::ifstream keyFile(this->_dictionaryFileName);
	char arr[ENGLISH_LETTERS_NUM] = { 0 };

	if (keyFile.is_open())
	{
		while (std::getline(keyFile, line))
		{ // initializing the array with values in each line
			arr[line[0] - 'a'] = line[2];
		}
		keyFile.close();
	}
	// for example: { r, o, v, p...}
	// r is refered a, o is refered to b, etc.

	// encrypt with the values in the array
	for (unsigned int i = 0; i < (this->_text).length(); i++)
	{
		if (_text[i] >= 'a' && _text[i] <= 'z')
		{
			encrypted += arr[_text[i] - 'a'];
		}
		else {
			encrypted += _text[i];
		}
	}
	this->_isEncrypted = true;
	this->_text = encrypted;

	return encrypted;
}

std::string SubstitutionText::decrypt()
{
	int temp = 0;
	std::string fileName = this->_dictionaryFileName;
	std::string str = FileHelper::readFileToString(this->_dictionaryFileName); // reading dict to string
	this->_dictionaryFileName = REV_FILE;

	reverse(str.begin(), str.end() - 1); // reversing the string
	std::ofstream revFile(REV_FILE); // creating a new file for the reversed string
	revFile << str;
	revFile.close();
	this->encrypt(); // decrypt (because of the new reversed file we created)

	remove(REV_FILE); // removing the file
	this->_dictionaryFileName = fileName;

	this->_isEncrypted = false;

	return _text;
}

std::ostream& operator<<(std::ostream& out, SubstitutionText& sub)
{
	std::cout << "Substitution" << std::endl;

	if (!sub._isEncrypted)
	{
		out << sub.encrypt();
		sub.decrypt();
	}
	else {
		out << sub._text;
	}

	return out;
}
