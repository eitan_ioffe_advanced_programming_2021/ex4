#include "Caesar.h"

CaesarText::CaesarText(const std::string text) : ShiftText(text, CAESAR_KEY)
{ // shows a warning if key is not initialized
	this->_key = CAESAR_KEY;
}

CaesarText::~CaesarText()
{ // nothing to clear
}

std::string CaesarText::encrypt()
{
	return this->ShiftText::encrypt();
}

std::string CaesarText::decrypt()
{
	return this->ShiftText::decrypt();
}

std::ostream& operator<<(std::ostream& out, CaesarText& c)
{
	std::cout << "Caesar" << std::endl;

	if (!c._isEncrypted)
	{
		out << c.encrypt();
		c.decrypt();
	}
	else {
		out << c._text;
	}

	return out;
}
