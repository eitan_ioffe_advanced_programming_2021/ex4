#ifndef SUBSTITUTIONTEXT_H
#define SUBSTITUTIONTEXT_H

#include <iostream>
#include "PlainText.h"
#include "FileHelper.h"

#define REV_FILE "revFile.txt"

class SubstitutionText : public PlainText
{
private:
	std::string _dictionaryFileName;
public:
	SubstitutionText(const std::string text, const std::string dictionaryFileName);
	~SubstitutionText();
	std::string encrypt(); // return and change to encrypted string
	std::string decrypt(); // return and change to decrypted string

	friend std::ostream& operator<<(std::ostream& out, SubstitutionText& v); // return encrypted string
};

#endif